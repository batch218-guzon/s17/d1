console.log("Hello World!");

// [SECTION] Functions
// Functions in javascript are lines/block of codes that tell our device/ application to perform a certain task when called/invoked.
// They are also used to prevent repeating lines/block of codes that perform the same task/function

//Function Declaration
	// function statement defines a function with parameters

		//function keyword - used to define a javascript function
		//function name - is used so we are able to call/invoke a declared function
		//function code block({}) - the statement which comprise the body of the function. This is where the code to be executed.
// function declaration // function name
// function name requires open and close parenthesis beside it
function printName() { //code block
	console.log("My name is John"); //function statement
};//delimeter

printName(); // Function Invocation

// [HOISTING]
// Hoisting is JavaScript's behavior for certain variable and functions to run or use before their declaration

declaredFunction();
//make sure the function is existing whenever we call/invoke a function

	function declaredFunction() {
		console.log("Hello world");
	};

declaredFunction();

// [Function Expression]
// A function can also be stored in a variable. That is called a function expression
// Hoisting is allowed in function declaration, while, we could not hoist through function expression 

let variableFunction = function() {
	console.log("Hello again!");
};

variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side");
};

funcExpression();
// funcName();
// Error: whenever we are calling a named function stored in a variable, we just call the variable name, not the function name

console.log("----------------------");
console.log("[Reassigning functions]");

declaredFunction();

declaredFunction = function() {
	console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function() {
	console.log("updated funcExpression");
};

funcExpression();

// Constant function
const constantFunction = function() {
	console.log("Initialized with const");
};

constantFunction();

/*constantFunction = function() {
	console.log("New value");
};

constantFunction();*/
// function with const keyword cannot be reassigned
// scope is the accessibility / visibility of a variables in the code

/*
	Javascript variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/

console.log("--------------------");
console.log("[Function Scoping]");

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

// console.log(localVar);

let globalVar = "Mr. Worldwide";
console.log(globalVar);

{
	console.log(globalVar);
}

// [Function Scoping]
// variables defined inside a function are not accessible/visible outside the function 
// variables declared with var, let, and const are quite similar when declared inside a function

function showNames() {
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);

// Nested Functions
	// you can create another function inside a function, called nested function

function myNewFunction() {
	let name = "Jane";

	function nestedFunction() {
		let nesteName = "John";
		console.log(name);
	};
	nestedFunction(); // result not defined

	console.log(nestedFunction);
};

myNewFunction();

// Global Scoped Variable
let globalName = "Zuitt";

function myNewFunction2() {
	let nameInside = "Renz";
	console.log(globalName);
};

myNewFunction2();

// alert() allows us to show a small window at the top of our browser page to show information to our users.
// alert("Sample Alert");


function showSampleAlert() {
	alert("Hello, User");
};

showSampleAlert();

// alert messages inside a function will only execute whenever we call/invoke the function

console.log("i will only log in the console when the alert is dismissed");

// Notes on the use of alert();
	// Show only an alert for short dialogs/messages to the user
	// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [Prompt]
	//prompt() allows us to show small window at the top of the browser to gather user input.
	// let samplePrompt = prompt("Enter your full name");
	// console.log(samplePrompt);
	// console.log(typeof samplePrompt);
	// console.log("Hello, " + samplePrompt);

	function printWelcomeMessage() {
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");
	
		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("welcome to my page!");
	}

	printWelcomeMessage();



	

	// [SECTION] Function Naming Convention

	// Function name should be definitive of the task it will perform. It usually contains a verb

	function getCourses() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	};

	getCourses();

	// Avoid generic names to avoid cinfusion within our code.

	function get() {
		let name = "Jamie";
		console.log(name);
	};

	get();

	// avoid pointless and inappropriate funstion names, example: foo, bar, etc.

	function foo() {
		console.log(25 % 5);
	};
	foo();

	// name your function in small caps. Follow camelCase when naming variables and functions

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	};

	displayCarInfo();